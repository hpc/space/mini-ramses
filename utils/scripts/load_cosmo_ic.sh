mkdir ics_ramses
cd ics_ramses
curl https://tigress-web.princeton.edu/~rt3504/DAT/ics_ramses/ic_deltab --output ic_deltab
curl https://tigress-web.princeton.edu/~rt3504/DAT/ics_ramses/ic_velcx  --output ic_velcx
curl https://tigress-web.princeton.edu/~rt3504/DAT/ics_ramses/ic_velcy  --output ic_velcy
curl https://tigress-web.princeton.edu/~rt3504/DAT/ics_ramses/ic_velcz  --output ic_velcz
cd ..

