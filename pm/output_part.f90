module output_part_module
contains
!#######################################################
!#######################################################
!#######################################################
!#######################################################
recursive subroutine r_output_part(pst,input_array,input_size,output_array,output_size)
  use mdl_module
  use amr_parameters, only: flen
  use ramses_commons, only: pst_t
  use mdl_parameters
  implicit none
  type(pst_t)::pst
  integer,VALUE::input_size
  integer::output_size
  integer,dimension(1:input_size)::input_array
  integer,dimension(1:output_size)::output_array
  
  character(LEN=flen)::filename,filename2
  integer::rID

  if(pst%nLower>0)then
     rID = mdl_send_request(pst%s%mdl,MDL_OUTPUT_PART,pst%iUpper+1,input_size,output_size,input_array)
     call r_output_part(pst%pLower,input_array,input_size,output_array,output_size)
     call mdl_get_reply(pst%s%mdl,rID,output_size)
  else
     filename=transfer(input_array,filename)
     if(index(filename,'output')==0)then
        filename2=TRIM(filename)//'part.'
        call backup_part(pst%s%r,pst%s%g,pst%s%p,filename2)
        if(pst%s%r%star)then
           filename2=TRIM(filename)//'star.'
           call backup_part(pst%s%r,pst%s%g,pst%s%s,filename2)
        endif
     else
        filename2=TRIM(filename)//'part.'
        call output_part(pst%s%r,pst%s%g,pst%s%p,filename2)
        if(pst%s%r%star)then
           filename2=TRIM(filename)//'star.'
           call output_part(pst%s%r,pst%s%g,pst%s%s,filename2)
        endif
     endif
  endif

end subroutine r_output_part
!#######################################################
!#######################################################
!#######################################################
!#######################################################
subroutine output_part(r,g,p,filename)
  use amr_parameters, only: ndim,dp,i8b,flen
  use hydro_parameters, only: nvar
  use amr_commons, only: run_t,global_t
  use pm_commons, only: part_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(part_t)::p
  character(LEN=flen)::filename

  integer::i,idim,ilun,ierr
  character(LEN=flen)::fileloc
  character(LEN=5)::nchar
  real(kind=4),allocatable,dimension(:)::xsp
  integer(i8b),allocatable,dimension(:)::ii8
  integer,allocatable,dimension(:)::ll
  logical::file_exist

  ilun=10

  call title(g%myid,nchar)
  fileloc=TRIM(filename)//TRIM(nchar)
  inquire(file=fileloc, exist=file_exist)
  if (file_exist) then
     open(unit=ilun,file=fileloc,iostat=ierr)
     close(ilun,status="delete")
  end if
  open(unit=ilun,file=TRIM(fileloc),access="stream",action="write",form='unformatted')
  rewind(ilun)
  ! Write header
  write(ilun)ndim
  write(ilun)p%npart
  ! Write position
  allocate(xsp(1:p%npart))
  do idim=1,ndim
     do i=1,p%npart
        xsp(i)=p%xp(i,idim)
     end do
     write(ilun)xsp
  end do
  ! Write velocity
  do idim=1,ndim
     do i=1,p%npart
        xsp(i)=p%vp(i,idim)
     end do
     write(ilun)xsp
  end do
  ! Write mass
  do i=1,p%npart
     xsp(i)=p%mp(i)
  end do
  write(ilun)xsp
  ! Write metalicity
  if(allocated(p%zp))then
     do i=1,p%npart
        xsp(i)=p%zp(i)
     end do
     write(ilun)xsp
  endif
  ! Write birth time
  if(allocated(p%tp))then
     do i=1,p%npart
        xsp(i)=p%tp(i)
     end do
     write(ilun)xsp
  endif
  deallocate(xsp)
  ! Write identity
  allocate(ii8(1:p%npart))
  do i=1,p%npart
     ii8(i)=p%idp(i)
  end do
  write(ilun)ii8
  deallocate(ii8)
  ! Write level
  allocate(ll(1:p%npart))
  do i=1,p%npart
     ll(i)=p%levelp(i)
  end do
  write(ilun)ll
  deallocate(ll)

#ifdef OUTPUT_PARTICLE_POTENTIAL
  ! Write potential (optional)
  allocate(xsp(1:p%npart))
  do i=1,p%npart
     xsp(i)=p%phip(i)
  end do
  write(ilun)xsp
  deallocate(xsp)
#endif

  close(ilun)

end subroutine output_part
!#######################################################
!#######################################################
!#######################################################
!#######################################################
subroutine backup_part(r,g,p,filename)
  use amr_parameters, only: ndim,dp,i8b,flen
  use hydro_parameters, only: nvar
  use amr_commons, only: run_t,global_t
  use pm_commons, only: part_t
  implicit none
  type(run_t)::r
  type(global_t)::g
  type(part_t)::p
  character(LEN=flen)::filename

  integer::i,idim,ilun,ierr
  character(LEN=flen)::fileloc
  character(LEN=5)::nchar
  real(dp),allocatable,dimension(:)::xdp
  integer(i8b),allocatable,dimension(:)::ii8
  integer,allocatable,dimension(:)::ll
  logical::file_exist

  ilun=10

  call title(g%myid,nchar)
  fileloc=TRIM(filename)//TRIM(nchar)
  inquire(file=fileloc, exist=file_exist)
  if (file_exist) then
     open(unit=ilun,file=fileloc,iostat=ierr)
     close(ilun,status="delete")
  end if
  open(unit=ilun,file=TRIM(fileloc),access="stream",action="write",form='unformatted')
  rewind(ilun)
  ! Write header
  write(ilun)ndim
  write(ilun)p%npart
  ! Write position
  allocate(xdp(1:p%npart))
  do idim=1,ndim
     do i=1,p%npart
        xdp(i)=p%xp(i,idim)
     end do
     write(ilun)xdp
  end do
  ! Write velocity
  do idim=1,ndim
     do i=1,p%npart
        xdp(i)=p%vp(i,idim)
     end do
     write(ilun)xdp
  end do
  ! Write mass
  do i=1,p%npart
     xdp(i)=p%mp(i)
  end do
  write(ilun)xdp
  ! Write metalicity
  if(allocated(p%zp))then
     do i=1,p%npart
        xdp(i)=p%zp(i)
     end do
     write(ilun)xdp
  endif
  ! Write birth time
  if(allocated(p%tp))then
     do i=1,p%npart
        xdp(i)=p%tp(i)
     end do
     write(ilun)xdp
  endif
  deallocate(xdp)
  ! Write identity
  allocate(ii8(1:p%npart))
  do i=1,p%npart
     ii8(i)=p%idp(i)
  end do
  write(ilun)ii8
  deallocate(ii8)
  ! Write level
  allocate(ll(1:p%npart))
  do i=1,p%npart
     ll(i)=p%levelp(i)
  end do
  write(ilun)ll
  deallocate(ll)

  close(ilun)

end subroutine backup_part
!#######################################################
!#######################################################
!#######################################################
!#######################################################
end module output_part_module
